const db = require('mysql2/promise')

class Database {
    constructor() {
        this.connectDatabase()
    }
    async connectDatabase() {
        this.connection = await db.createConnection({
            host: 'localhost',
            user: 'root',
            database: 'codecamp'
        })
    }
    async execute(sql){
        const [row, fields] = await this.connection.execute('SELECT * FROM user')
        return row
    }
}

module.exports.db = new Database()