const Koa = require('koa')
const logger = require('koa-logger')
const winston = require('winston')
const app = new Koa()
const render = require('koa-ejs')
const path = require('path')
require('./controllers/routes.js')(app)

app.use(logger())

const winLog = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    transports: new winston.transports.File({
            filename: 'logger/error.log',
            level: 'error'
        },
        new winston.transports.File({
            filename: 'logger/combined.log',
            level: 'info'
        }))
})

winLog.add(new winston.transports.Console({
    format: winston.format.simple()
}))
render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'templates',
    viewExt: 'ejs',
    cexhe: false,
    debub: true
})

app.use(async(ctx, next) => {
    try {

    } catch (err) {
        ctx.status = 400
        ctx.body = `OMG-Error ${err.message}`
        winLog.error(`${err.message}`)
    }
})

winLog.info(`This is info log `)

app.listen(3000)