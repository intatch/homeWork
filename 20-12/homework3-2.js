const fs = require('fs')

let readHead = new Promise(function (resolve, rejuct) {
    fs.readFile('head.txt', 'utf8', function (err, dataHead) {
        if (err)
            reject(err)
        else
            resolve(dataHead + '\n')
    })
})
let readBody = new Promise(function (resolve, rejuct) {
    fs.readFile('body.txt', 'utf8', function (err, dataBody) {
        if (err)
            reject(err)
        else
            resolve(dataBody + '\n')
    })
})
let readLeg = new Promise(function (resolve, rejuct) {
    fs.readFile('leg.txt', 'utf8', function (err, dataLeg) {
        if (err)
            reject(err)
        else
            resolve(dataLeg + '\n')
    })
})
let readFeet = new Promise(function (resolve, rejuct) {
    fs.readFile('feet.txt', 'utf8', function (err, dataFeet) {
        if (err)
            reject(err)
        else
            resolve(dataFeet + '\n')
    })
})

function writeRobot(result) {
    return new Promise(function (resolve, reject) {
        fs.writeFile('robotH2.txt', result, 'utf8',function (err) {
            if (err)
            reject(err);
            else
            resolve("Promise Success!");
        });
    });
}

async function copyFile() {// ชื่อ
    try {
        let dataHead = await readHead //ให้ readh ทำงานก่อน แล้ว return datah แล้วกำหนดใน datah
        let dataBody = await readBody
        let dataLeg = await readLeg
        let dataFeet = await readFeet
        writeRobot(dataHead+dataBody+dataLeg+dataFeet)
    } catch (error) {
        console.error(error);
    }
}
copyFile();