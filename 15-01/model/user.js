
class User {
    constructor(db, rows) {
        this._db = db
        this._id = rows.id
        this.firstname = rows.firstname
        this.lastname = rows.lastname
    }

    async save() {
        if (!this._id) {
            const result = await this._db.execute(`insert into user ( firstname, lastname ) values ( ?, ? )`, [this.firstname, this.lastname])
            this._id = result.insertId
            return
        }
        
        
        return this._db.execute(`update user set firstname = ?, lastname = ? where id = ?`, [this.firstname, this.lastname, this._id])
    }

    remove() {
        return this._db.execute(` delete from user where id = ? `, [this._id])
    }
    async store(){
        if(!this._id){
            const result = await this
        }
    }
}

module.exports = (async(db) => {
    

    return {
        async find(id) {
            const [rows] = await db.execute(`select id, firstname, lastname from user where id = ?`, [id])
            return new User(db, rows[0])
        },
        async findAll() {
            const [rows] = await db.execute(`select id, firstname, lastname from user`)
            return rows
        },
        async findByUsername(firstname) {
            const [rows] = await db.execute(`select id, firstname, lastname from user where firstname = ?`, [firstname])
            return rows
        }
    }

})