const mysql = require('mysql2/promise')
module.exports = mysql.createPool({
    host: '127.0.0.1',
    user: 'root',
    database: 'twitter'
})