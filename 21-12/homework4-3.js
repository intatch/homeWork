const fs = require('fs')

function upSalary(Salaryman) {
    let salary = parseInt(Salaryman.salary)
    if (Salaryman.salary < 1e5) {
        Salaryman.salary = salary * 2
    }else{
        Salaryman.salary = salary
    }
    console.log(Salaryman)
    return Salaryman
}

function allSalary(Salary,sam){
    let salary = Salary.salary
    return salary
}

fs.readFile('homework1.json', 'utf8', (err, data) => {
    let jsondata = JSON.parse(data)
    let chainingSalaryman = jsondata.map(upSalary)
    .map(allSalary)
    .reduce((salary, sum) => {
        return salary + sum
    }, 0)
    console.log(chainingSalaryman)
})