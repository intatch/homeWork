const fs = require('fs')

function mapFirstlastname(Student){
    Student.Fullname = Student.firstname + " " + Student.lastname
    let salary = parseInt(Student.salary) 
    let salaryOne = salary * 0.1
    let salaryOneUp = salaryOne + salary

    let salaryTwo = salaryOneUp * 0.1
    let salaryTwoUp = salaryTwo + salaryOneUp

    let salaryTree = salaryTwo * 0.1
    let salaryTreeUp = salaryTree + salaryTwoUp

    Student.salary = [salaryOneUp, salaryTwoUp, salaryTreeUp]
    return Student
}

fs.readFile('homework1.json', 'utf8', (err, data) => {
    let jsonStudents = JSON.parse(data)
    
    let chaining = jsonStudents.map(mapFirstlastname)
    console.log(chaining)
    
})
