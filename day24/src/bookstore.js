export function create(isbn, title, price, imgUrl) {
    $("#newRows").append(`
    <div class="col-sm-5 col-md-3" id="${isbn}">
    <div class="thumbnail">
        <h4 class="text-center">${title}</h4>
        <img class="img-responsive" src="${imgUrl}" alt="..." style="height:400px;">
        <div class="caption text-center">
            <p>
                <p>Price: ${price}$
                </p>
            </p>
            <p>
            <button onclick="window.setIDEdit(${isbn})" bookId="${isbn}" type="button" class="btn btn-warning" data-toggle="modal" data-target="#ModalEdit">Edit</button>
            <button  id="delete" bookId="${isbn}" type="button" class="btn btn-danger" data-toggle="modal" data-target="#alert">delete</button>
            </p>
        </div>
    </div>
</div>
    `)
}

export function edit(isbn, title, price, imgUrl) {
    $(`div #${isbn} div h4`).html(`${title}`)
    $(`div #${isbn} div img`).attr("src", `${imgUrl}`)
    $(`div #${isbn} div div `).html(`
    <div class="caption text-center">
            <p>
                <p>Price: ${price}$
                </p>
            </p>
            <p>
            <button onclick="window.setIDEdit(${isbn})" id="editItem" bookId="${isbn}" type="button" class="btn btn-warning" data-toggle="modal" data-target="#edit">Edit</button>
            <button  id="delete" bookId="${isbn}" type="button" class="btn btn-danger" data-toggle="modal" data-target="#alert">delete</button>
            </p>
        </div>
    `)
}

export function remove2(isbn) {
    $(`#${isbn}`).remove()
}