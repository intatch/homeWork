module.exports = function (pool, repo) {
    return {
        async list(ctx){
            ctx.body = await repo.list(pool)
        },
        async create (ctx){
            let { ISBN, title, price, imgUrl} = ctx.request.body
            await repo.create(pool, ISBN, title, price, imgUrl)
            // console.log(ctx.request.body)
        },
        async update (ctx){
            let ISBN = ctx.params.id
            let { title, price, imgUrl} = ctx.request.body
            // console.log(ISBN, title, price, imgUrl)
            await repo.edit(pool, ISBN, title, price, imgUrl)
        },
        async remove (ctx){
            let id = ctx.params.id
            // console.log(id)
            await repo.remove(pool, id)
        }
    }
}