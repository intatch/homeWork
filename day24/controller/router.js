const Router = require('koa-router')
const makeCtrl = require('../controller/book')
const repo = require('../repo/useBook')
const dbConfig = require('../config/db.json')
const mysql = require('mysql2/promise')

const pool = mysql.createPool({
    "host": "127.0.0.1",
    "user": "root",
    "database": "bookstore1"
})

const ctrlBook = makeCtrl(pool, repo)

const router = new Router()
    .get('/', ctrlBook.list)
    .post('/add', ctrlBook.create)
    .patch('/edit/:id', ctrlBook.update)
    .delete('/delete/:id', ctrlBook.remove)

module.exports = function (app){
    app.use(router.routes())
}