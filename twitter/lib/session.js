const session = require('koa-session')

let sessionStore = {}

const sessionConfig = {
    key: 'sess',
    maxAge: 3600 * 10e3,
    httpOnly: true,
    store: {
        get(key, maxAge, {rolling}) { //นับอายุ maxage ใหม่
            return sessionStore[key]
        },
        set(key, sess, maxAge, {rolling}) {
            sessionStore[key] = sess
        },
        destroy(key) {
            delete sessionStore[key]
        }
    }
}


module.exports = (app) => {
    app.keys = ['supersecret']
    app.use(session(sessionConfig, app)) 
}
