module.exports = (pool, repo) => {
    return {
        async list(ctx) {
            ctx.body = 'list'
        },
        async create(ctx) {
            ctx.body = 'create'
        },
        async like(ctx) {
            ctx.body = 'like'
        },
        async unLike(ctx) {
            ctx.body = 'unLike'
        },
        async reTweet(ctx) {
            ctx.body = 'reTweet'
        },
        async cratePoll(ctx) {
            ctx.body = 'cratePoll'
        },
        async createReply(ctx) {
            ctx.body = 'createReply'
        },
        async vote(ctx) {
            ctx.body = 'vote'
        }
    }
}