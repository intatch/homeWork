const {
    progammer
} = require('./progammer.js')
const {
    Employee
} = require('./employee.js');
const {OfficeCleaner} = require('./OfficeCleaner.js')
const fs = require('fs')
class CEO extends Employee {
    constructor(firstname, lastname, salary, id, dressCode) {
        super(firstname, lastname, salary);
        this.dressCode = dressCode;
        this.id = id
        this.dressCodeAtOffice = 'tshirt'
        this.employees = []
        this.employee9 = []
        this.emploProgammer = []
        this.emploCEO = []
        this.emploOff = []
    }
    readFilefunc() {
        return new Promise((resolve, reject) => {
            fs.readFile('homework.json', 'utf-8', (err, data) => {
                if (err) reject(err)
                else resolve(JSON.parse(data))
            })
        })
    }
    instance(row) {
        row.forEach(person => {
            let newClass = new progammer(person.firstname, person.lastname, person.salary, person.id)
            this.employees.push(newClass)
        })
    }
    async readfileAll() {
        try {
            let data = await this.readFilefunc()

            this.instance(data)
        } catch (err) {
            console.log(err)
        }
    }

    getSalary() { // simulate public method
        return super.getSalary() * 2;
    };
    work(employee) { // simulate public method
        this._fire(employee)
        this._hire(employee)
        this._seminar()
        this._golf()

    }
    _fire(employee) {
        console.log(employee.firstname + ' has been fired! Dress with ' + this.dressCodeAtOffice)
    }
    _hire(employee) {
        console.log(employee.firstname + ' has been hired back! Dress with ' + this.dressCodeAtOffice)
    }
    _seminar() {
        console.log('He is going to seminar Dress with ' + this.dressCode)
    }
    increaseSalary(employee, newSalary) {
        if (employee.setSalary(newSalary)) {
            console.log(employee.firstname + ' salary has been set to ' + newSalary)
        } else {
            console.log(employee.firstname + ' salary is less than before!!! ' + newSalary)
        }
    } //

    _golf() { // simulate private method
        this.dressCode = 'golf_dress';
        console.log("He goes to golf club to find a new connection." + " Dress with :" + this.dressCode);
    };

    talk(message) {
        console.log(message)
    }
    reportRobot(self, robotMessage) {
        self.talk(robotMessage)
    }
    // reportRobot(robotMessage){
    //     this.talk(robotMessage)
    // }

    readFileEmplo9() {
        return new Promise((resolve, reject) => {
            fs.readFile('employee9.json', 'utf-8', (err, data) => {
                if (err) reject(err)
                else resolve(JSON.parse(data))
            })
        })
    }
    async all() {
        try {
            this.employee9 = await this.readFileEmplo9()
            await this.employee9.forEach(person =>{
                this.aaaa(person)
            })

        } catch (err) {
            console.log(err)
        }
    }
    aaaa(person) {
        if (person.role === "CEO") {
            person.firstname = new CEO(person.firstname, person.lastname, person.salary, person.id, person.dressCode)
            this.emploCEO.push(person.firstname)
        } else if (person.role === "Programmer") {
            person.firstname = new progammer(person.firstname, person.lastname, person.salary, person.id, person.type)
            this.emploProgammer.push(person.firstname)
        } else if (person.role === "OfficeCleaner") {
            person.firstname = new OfficeCleaner(person.firstname, person.lastname, person.salary, person.id, person.dressCode)
            this.emploOff.push(person.firstname)
        }
    }
}
exports.CEO = CEO;