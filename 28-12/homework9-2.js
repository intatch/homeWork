const {OfficeCleaner} = require('./OfficeCleaner')
const fs = require('fs')
const {
    CEO
} = require('./CEO')
const {
    progammer
} = require('./progammer')

let somchai = new CEO('somchai', 'somchai', 30000, 1234, 'suit')
let somshi = new OfficeCleaner('somshi', 'moomu', 100000, 123, 'tshirt')
async function raw() {
    try {
        let data = await somchai.all()
        somchai.emploProgammer.map(x => {
            return x.work()
        })
        somchai.emploCEO.map(x => {
            return x.work(somshi)
        })
        somchai.emploOff.map(x => {
            return x.work()
        })
    } catch (err) {
        console.log(err)
    }
}

raw()