class Employee {
    constructor(firstname, lastname, salary) {
        this._salary = salary; // simulate private variable
        this.firstname = firstname
        this.lastname = lastname
    }
    setSalary(newSalary) { // simulate public method
        // return newSalary ถ้ามีเงินเดือนใหม่มีค่ามากกว่า this._salary
        // return false ถ้าเงินเดือนใหม่มีค่าน้อยกว่าเท่ากับ this._salary
        if( this._salary < newSalary){
            newSalary = this._salary
            return newSalary
        }else{
            return false
        }
        
    }
    gossip(employee, srting) {
        console.log('Hey ,' + employee.firstname + " " + srting)
    }
    getSalary() { // simulate public method
        return this._salary;
    };
    work(employee) {
        // leave blank for child class to be overidden
    }
    leaveForVacation(year, month, day) {

    }

}
exports.Employee = Employee;