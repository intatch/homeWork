const Koa = require('koa')
const app = new Koa()
const logger = require('koa-logger')
const winston = require('winston')
const render = require('koa-ejs')
const path = require('path')
const otherMiddleware = require('./controller/routes.js')(app)
const db = require('./lib/db.js')()

app.use(logger())

const winLog = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    transports: [
        new winston.transports.File({
            filename: 'logger/error.log',
            level: 'error'
        }),
        new winston.transports.File({
            filename: 'logger/combined.log',
            level: 'info'
        })
    ]
})

winLog.add(new winston.transports.Console({
    format: winston.format.simple()
}))

render(app, {
    root: path.join(__dirname, 'views', ),
    layout: 'template',
    viewExt: 'ejs',
    cexhe: false,
    debug: true
})

app.use(async(ctx, next) => {
    try {
        let data = await db.getConnection()
        await ctx.render('homework10-1', {"message": data})
        await next()
    } catch (err) {
        ctx.status = 400
        ctx.body = `Uh-oh: ${err.message}`
        winLog.error(`${err.message}`)
    }
})

winLog.info(`This is info log `, )


app.listen(3000)