const mysql = require('mysql2/promise')
const dbConfig = require('../config/db.json')

module.exports = mysql.createPool(dbConfig)