const koa = require('koa')
const multer = require('koa-multer')
const fs = require('fs')
const jimp = require('jimp')
const mysql = require('mysql2/promise')
const app = new koa()
const upload = multer({
    dest: 'upload/'
})

app.use(async(ctx) => {
    const pool = mysql.createPool({
        host: '127.0.0.1',
        user: "root",
        database: "testtwitter"
    })
    const db = await pool.getConnection()
    await upload.single('file')(ctx)
    const temp = ctx.req.file.path
    const outFile = temp + '.jpg'
    const imgNew = await jimp.read(temp)
    imgNew.resize(500, 500)
        .write(outFile)
    await db.execute(`insert into img (text, user_id) values (?,?)`, [outFile, 1])
    fs.unlink(temp, () => {})
    ctx.body = outFile
})

app.listen(3000)